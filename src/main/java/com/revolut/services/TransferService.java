package com.revolut.services;

import com.revolut.model.request.TransferRequest;
import com.revolut.model.response.TransferResponse;

public interface TransferService {
	TransferResponse transfer(TransferRequest request);

	TransferResponse transferSchedule(TransferRequest request);

	TransferResponse findTransferByTransId(Integer transId);
}
