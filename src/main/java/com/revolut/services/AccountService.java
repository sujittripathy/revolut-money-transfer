package com.revolut.services;

import com.revolut.model.request.AccountRequest;
import com.revolut.model.response.AccountResponse;
import com.revolut.model.response.AccountResponseList;

public interface AccountService {
	AccountResponse addAccount(AccountRequest account);

	AccountResponseList fetchAllAccounts();

	AccountResponse findOneAccount(String accountNumber);

	AccountResponse deposit(AccountRequest account);

	AccountResponse withdraw(AccountRequest account);

	AccountResponse close(String accountNumber);
}
