package com.revolut.exception;

import com.revolut.util.ErrorEnum;

public class AccountNotFoundException extends MoneyTransferBaseException {
	public AccountNotFoundException(ErrorEnum errorEnum) {
		super(errorEnum.getMessage(), errorEnum.getCode(), errorEnum.getType());
	}
}
