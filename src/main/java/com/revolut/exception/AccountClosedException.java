package com.revolut.exception;

import com.revolut.util.ErrorEnum;

public class AccountClosedException extends MoneyTransferBaseException {
	public AccountClosedException(ErrorEnum errorEnum) {
		super(errorEnum.getMessage(), errorEnum.getCode(), errorEnum.getType());
	}
}
