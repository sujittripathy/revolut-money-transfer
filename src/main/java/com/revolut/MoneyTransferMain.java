package com.revolut;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.revolut.configuration.APIConfiguration;
import com.revolut.api.routes.AccountRoutes;
import com.revolut.api.routes.TransferRoutes;
import com.revolut.configuration.GuiceModule;
import com.revolut.model.Validator;
import com.revolut.services.AccountServiceImpl;
import com.revolut.services.TransferServiceImpl;

public class MoneyTransferMain {

	public static void main(String[] args) throws Exception {
		initializeInjectors();
		//Initialize API Configuration
		new APIConfiguration();
	}

	private static void initializeInjectors() {
		//Initialize DI modules
		Injector injector = Guice.createInjector(new GuiceModule());
		injector.getInstance(AccountRoutes.class);
		injector.getInstance(TransferRoutes.class);
		injector.getInstance(AccountServiceImpl.class);
		injector.getInstance(TransferServiceImpl.class);
		injector.getInstance(Validator.class);
	}
}
