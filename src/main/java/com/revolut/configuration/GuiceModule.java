package com.revolut.configuration;

import com.google.inject.AbstractModule;
import com.revolut.db.MoneyTransferDAO;
import com.revolut.db.MoneyTransferDAOImpl;
import com.revolut.model.Validator;
import com.revolut.services.AccountService;
import com.revolut.services.AccountServiceImpl;
import com.revolut.services.TransferService;
import com.revolut.services.TransferServiceImpl;

public class GuiceModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(AccountService.class).to(AccountServiceImpl.class);
		bind(TransferService.class).to(TransferServiceImpl.class);
		bind(MoneyTransferDAO.class).to(MoneyTransferDAOImpl.class);
		bind(Validator.class);
	}
}
