package com.revolut.configuration;

import com.revolut.api.filter.AfterFilter;
import com.revolut.api.handler.ExceptionHandler;
import com.revolut.api.routes.AccountRoutes;
import com.revolut.api.routes.TransferRoutes;

public class APIConfiguration {
	public APIConfiguration() throws Exception {
		Class[] classes = {AccountRoutes.class, TransferRoutes.class,
								ExceptionHandler.class, AfterFilter.class};
		for(Class clazz: classes) {
			clazz.getDeclaredConstructors()[0].newInstance();
		}
	}
}
